


ai = require 'ai'


devs =
	ph5: { title: 'iPhone 5', file: '4-Inch' }
	ph4: { title: 'iPhone 4', file: '3.5-Inch' }
	pd:  { title: 'iPad', file: 'iPad' }

langs =
	en: { title: 'English', file: 'English' }
	ru: { title: 'Russian', file: 'Russian' }



class Snapshots

	constructor: () ->
		@app = ai.ios_app()
		@errors = []
		return





	set_lang: (lang_name) ->

		loc_visible = (lang_name, dev_name, snapshot_name, name) =>
			for enum_lang_name of langs
				n = "#{dev_name} #{snapshot_name} #{name} #{enum_lang_name}"
				try
					item = activeDocument.pageItems.getByName n
					item.hidden = enum_lang_name isnt lang_name
				catch e
					@errors.push "#{n} not found"
			return

		loc_contents = (lang_name, dev_name, snapshot_name, name, contents) =>
			name = "#{dev_name} #{snapshot_name} #{name}"
			try
				item = activeDocument.pageItems.getByName name
				item.contents = contents[lang_name]
			catch e
				@errors.push "#{name} not found"
			return

		# main

		for dev_name of devs
			for snapshot_name of @app.itc_snapshots
				snapshot = @app.itc_snapshots[snapshot_name]
				for item_name of snapshot
					if item_name is '_v'
						for i in [0 ... snapshot._v.length]
							loc_visible lang_name, dev_name, snapshot_name, snapshot._v[i]
					else
						loc_contents lang_name, dev_name, snapshot_name, item_name, snapshot[item_name]
		return





	export: () ->

		save_image = (lang_name, dev_name, snapshot_name) =>
			artboard_name = "#{dev_name} #{snapshot_name}"
			for artboard_index in [0 ... activeDocument.artboards.length]
				if activeDocument.artboards[artboard_index].name is artboard_name
					activeDocument.artboards.setActiveArtboardIndex artboard_index
					l = langs[lang_name]
					d = devs[dev_name]
					ai.export_png_24 1, "#{@app.itc_snapshots_path}/#{l.file}  #{d.file}  #{snapshot_name}.png"

					# options = new ExportOptionsPNG8
					# options.colorCount = 128
					# options.antiAliasing = true
					# options.artBoardClipping = true

					# # options.horizontalScale = 100*image.width/(r[2] - r[0]);
					# # options.verticalScale = 100*image.height/(r[1] - r[3]);
					# file = new File "#{app.itc_snapshots_path}/#{l.file}  #{d.file}  #{snapshot_name}.png"
					# activeDocument.exportFile file, ExportType.PNG8, options
			return

		totals = 0
		for lang_name of langs
			for dev_name of devs
				for snapshot_name of @app.itc_snapshots
					++totals

		progress = new Progress 'Export App Store Shapshots', totals
		try
			for lang_name of langs
				@set_lang lang_name
				for dev_name of devs
					for snapshot_name of @app.itc_snapshots
						progress.next "#{langs[lang_name].title}: #{devs[dev_name].title} (#{snapshot_name})"
						save_image lang_name, dev_name, snapshot_name
		finally
			progress.close()

		return





exports.set_lang = (lang) -> 
	(new Snapshots).set_lang lang
	return

exports.export_snapshots = () -> 
	(new Snapshots).export() 
	return



